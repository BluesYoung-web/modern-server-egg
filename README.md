# 基于 modernjs 创建的 RESTFUL API 的 node 服务端程序

[配套后台模板](https://gitee.com/BluesYoung-web/young-naive-admin-restful)

## 环境准备

`node lts`

```bash
# 安装依赖
yarn

# 运行项目
yarn dev

# 打包
yarn build
# 运行打包后的代码
yarn start
```

## 简介

代码全部位于 `./api` 目录下

```
├───app // 结构同 egg，可自由扩展
│   ├───entity // typeorm 的实体(表)
│   ├───extend // 对于 egg 的扩展
│   ├───middleware // 中间件
│   ├───middleware // 存放数据库迁移文件
│   ├───subscriber // 存放数据库订阅文件
│   └───schedule // 定时任务
│   └───service // 基础的数据库操作
├───config // 存放配置文件
├───lambda // bff 函数式写法，会映射为 RESTFUL API
└───typings // 自动生成的类型声明文件
```

## 环境变量

```bash
# 项目根目录新建 .env.local 文件，内容如下：

# 加密秘钥，jwt | 密码 通用
YOUNG_SECRET_KEY = 'BluesYoung-web'
```

## 配置文件

```ts
// config.default.ts 存放默认的配置文件

// config.local.ts 存放对应环境的配置文件
/********** 树莓派 **********/
import type { EggAppConfig, PowerPartial } from 'egg';

export default (appInfo: EggAppConfig) => {
  const config = {} as PowerPartial<EggAppConfig>;
  /** 数据库配置 */
  config.mysql = {
    host: '192.168.31.216',
    username: 'root',
    password: 'my-secret-pw',
  };

  return config;
};
/********** other **********/
import type { EggAppConfig, PowerPartial } from 'egg';

export default (appInfo: EggAppConfig) => {
  const config = {} as PowerPartial<EggAppConfig>;
  /** 数据库配置 */
  config.mysql = {
    host: 'localhost',
    username: 'young',
    password: '123456',
  };

  return config;
};
```

## 定时任务

```ts
/*
 * @Author: zhangyang
 * @Date: 2022-06-18 13:49:09
 * @LastEditTime: 2022-06-19 16:56:05
 * @Description:
 */
import { Subscription } from 'egg';

export default class extends Subscription {
  // 通过 schedule 属性来设置定时任务的执行间隔等配置
  static get schedule() {
    return {
      // 1 分钟间隔，定时
      interval: '1m',
      // interval: '10s',

      // 每分钟开始的时候执行一次，准点
      /**
                *    *     *     *     *    *
               ┬    ┬    ┬    ┬    ┬   ┬
              │    │    │    │    │    |
             │    │    │    │    │   └ day of week (0 - 7) (0 or 7 is Sun)
            │    │    │    │    └───── month (1 - 12)
           │    │    │    └────────── day of month (1 - 31)
          │    │    └─────────────── hour (0 - 23)
         │    └──────────────────── minute (0 - 59)
        └───────────────────────── second (0 - 59, optional)
       */
      // cron: '0 */1 * * * *',

      // 指定所有的 worker 都需要执行
      type: 'all',
    };
  }

  // subscribe 是真正定时任务执行时被运行的函数
  async subscribe() {
    // this.logger.info('我是每分钟一次的定时任务');
    // const res_get = await this.ctx.curl('http://localhost:8080/api/hello', {
    //   dataType: 'json',
    // });
    // this.ctx.logger.info(res_get);
    // 添加 _csrf 之后 外部调用正常，内部还是 500
    // const res_post = await this.ctx.curl( `http://localhost:8080/api/hello?_csrf=${res_get.data.csrf}`, {
    //   // 通过 contentType 告诉 HttpClient 以 JSON 格式发送
    //   // contentType: 'json',
    //   // 返回的数据处理为 json
    //   dataType: 'json',
    //   method: 'POST',
    // });
    // this.logger.info(res_post);
  }
}
```
