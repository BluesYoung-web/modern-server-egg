/*
 * @Author: zhangyang
 * @Date: 2022-06-18 10:46:00
 * @LastEditTime: 2022-06-26 11:19:27
 * @Description:
 */
import { defineConfig } from '@modern-js/app-tools';

// https://modernjs.dev/docs/apis/config/overview
export default defineConfig({
  bff: {
    // 接口路径的前缀，默认为 /api
    // prefix: '/api/v1'
  },
  output: {
    // 关闭类型检测，防止内存溢出
    disableTsChecker: true,
  },
});
