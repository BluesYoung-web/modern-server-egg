/*
 * @Author: zhangyang
 * @Date: 2022-06-22 08:48:00
 * @LastEditTime: 2022-06-22 08:48:00
 * @Description:
 */
export default {
  jwt: {
    enable: true,
    package: 'egg-jwt',
  },
};
