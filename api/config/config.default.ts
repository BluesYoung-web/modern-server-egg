/*
 * @Author: zhangyang
 * @Date: 2022-06-18 10:54:58
 * @LastEditTime: 2022-06-26 14:52:38
 * @Description:
 */
import type { Context, EggAppConfig, PowerPartial } from 'egg';

export default (appInfo: EggAppConfig) => {
  const config = {} as PowerPartial<EggAppConfig>;
  config.keys = `${appInfo.name}123456`;
  config.middleware = ['trace', 'tokenHandler'];
  // 中间件执行匹配开启
  config.tokenHandler = {
    match(ctx: Context) {
      // 匹配不需要验证token的路由
      const { url } = ctx.request;
      const whiteList = ['/api/base', '/api/init'];
      if (whiteList.some(u => url.startsWith(u))) {
        ctx.logger.info('config.tokenHandler:', '关闭token验证');
        return false;
      } else {
        ctx.logger.info('config.tokenHandler:', '开启token验证');
        return true;
      }
    },
  };

  /**
   * 目前日志存放于 node_modules/.modernjs_egg/logs/[package.json.name]/*.log
   * 暂时无法通过配置修改
   */

  /** 日志文件分割 */
  config.logrotator = {
    enable: true,
    package: 'egg-logrotator',

    // 需要按小时切割的文件列表
    filesRotateByHour: ['modern-server-egg-web.log'],
    // 小时部分的分隔符
    hourDelimiter: '-',

    /*
    // 需要按大小切割的文件，其他日志文件仍按照通常方式切割
    filesRotateBySize: [],
    // 最大文件大小，默认为50m
    maxFileSize: 50 * 1024 * 1024,
    // 按大小切割时，文件最大切割的份数
    maxFiles: 10,
    // 按大小切割时，文件扫描的间隔时间
    rotateDuration: 60000,
    */
    // 日志保留时间(天数，设置为零则全部保存)
    maxDays: 7,
  };

  /** 数据库配置 */
  config.mysql = {
    port: 3306,
    database: 'dev_egg_demo',
    charset: 'utf8mb4',
    // 自动同步表结构
    synchronize: true,
    // sql 日志
    logging: false,
    entities: ['api/app/entity/*.ts'],
    migrations: ['api/app/migration/*.ts'],
    subscribers: ['api/app/subscriber/*.ts'],
  };

  /**
   * 自定义配置
   *   key 加盐加密
   */
  config._young = {
    key: process.env.YOUNG_SECRET_KEY,
  };

  /**
   * JWT 秘钥
   */
  config.jwt = {
    secret: process.env.YOUNG_SECRET_KEY,
  };

  return config;
};
