/*
 * @Author: zhangyang
 * @Date: 2022-06-20 10:27:04
 * @LastEditTime: 2022-06-20 11:20:30
 * @Description:
 */
import { DataSource } from 'typeorm';

const DATA_SOURCE = Symbol('Applicaiton#dataSource');

export default {
  get dataSource(): DataSource {
    // @ts-ignore 确保只实例化一次
    if (!this[DATA_SOURCE]) {
      // @ts-ignore
      const mysql_conf = this.config.mysql as any;
      // @ts-ignore
      this[DATA_SOURCE] = new DataSource({
        type: 'mysql',
        ...mysql_conf
      });
    }
    // @ts-ignore
    return this[DATA_SOURCE] as DataSource;
  }
};
