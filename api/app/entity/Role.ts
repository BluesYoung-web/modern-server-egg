/*
 * @Author: zhangyang
 * @Date: 2022-06-19 10:22:22
 * @LastEditTime: 2022-06-21 11:08:09
 * @Description:
 */
import { Column, Entity, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Api } from './Api';
import { BaseCreate } from './BaseCreate';
import { Menu } from './Menu';
import { User } from './User';

@Entity({
  engine: 'InnoDB',
})
export class Role extends BaseCreate {
  @Column({ length: '20' })
  name!: string;

  @Column({ length: '20' })
  keyword!: string;

  @Column({ default: '' })
  desc?: string;

  @OneToMany(() => User, user => user.role)
  users?: User[];

  @ManyToMany(() => Api, api => api.roles)
  @JoinTable()
  apis?: Api[];

  @ManyToMany(() => Menu, menu => menu.roles)
  @JoinTable()
  menus?: Menu[];
}
