/*
 * @Author: zhangyang
 * @Date: 2022-06-19 16:03:08
 * @LastEditTime: 2022-06-26 16:12:18
 * @Description:
 */
import { Service } from 'egg';
import { Column, PrimaryGeneratedColumn } from 'typeorm';
import type { Repository } from 'typeorm';
import type { Paginition } from 'api/typings/app/paginition';
/**
 * 基础表结构
 */
export abstract class BaseCreate {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id!: number;

  @Column({ update: false, type: 'datetime', default: () => 'NOW()' })
  createdAt?: string;

  @Column({ type: 'datetime', default: () => 'NOW()' })
  updateAt?: string;

  @Column({ length: '20', default: '' })
  creator?: string;

  @Column({ type: 'tinyint', default: 1 })
  status?: number;

  @Column({ default: 0 })
  sort?: number;
}

/**
 * 基类方法，为了不被自动识别，勉为其难的放在这
 */
export class YoungService<
  U extends BaseCreate,
  T extends Partial<U> = Partial<U>,
> extends Service {
  protected repo!: Repository<U>;

  async create(item: T) {
    this.logger.error('create method must be implement ---', item);
  }

  async createBatch(items: T[]) {
    let res: any = true;

    const queryRunner = this.app.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      for (const item of items) {
        await this.create(item);
      }
      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      res = error;
    } finally {
      await queryRunner.release();
    }

    if (res !== true) {
      throw res;
    }
  }

  async findOne(item: Partial<U>) {
    const query = this.repo.createQueryBuilder();
    query.select('*');
    for (const [key, value] of Object.entries(item)) {
      if (value) {
        query.andWhere(`${key} = :value`, { value });
      }
    }
    return query.getRawOne() as Promise<U | null>;
  }

  async findAll(item: Partial<U> & Paginition) {
    const query = this.repo.createQueryBuilder();
    query.select('*');
    for (const [key, value] of Object.entries(item)) {
      if (value) {
        query.andWhere(`${key} = :value`, { value });
      }
    }
    const pageNum = Number(item.pageNum) || 1;
    const pageSize = Number(item.pageSize) || 10;
    if (!item.noPagination) {
      const page = pageNum;
      const size = pageSize;

      const skip = (page - 1) * size;

      query.skip(skip);
      query.take(size);
    }

    const total = await query.getCount();
    const list = await query.getRawMany();

    return {
      pageNum,
      pageSize,
      total,
      list,
      noPagination: item.noPagination,
    };
  }

  async delete(id: number) {
    await this.repo.delete(id);
  }

  async deleteBatch(ids: number[]) {
    const queryRunner = this.app.dataSource.createQueryRunner();
    let res: any = true;

    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      for (const id of ids) {
        await this.delete(id);
      }
      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      res = error;
    } finally {
      await queryRunner.release();
    }

    if (res !== true) {
      throw res;
    }
  }
}
