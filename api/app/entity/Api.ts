/*
 * @Author: zhangyang
 * @Date: 2022-06-19 14:48:35
 * @LastEditTime: 2022-06-19 16:07:05
 * @Description:
 */
import { Column, Entity, ManyToMany } from 'typeorm';
import { BaseCreate } from './BaseCreate';
import { Role } from './Role';

@Entity({
  engine: 'InnoDB',
})
export class Api extends BaseCreate {
  @Column({ length: '20' })
  category!: string;

  @Column({ length: '30' })
  path!: string;

  @Column({ length: '10' })
  method!: string;

  @Column({ length: '20', default: '' })
  desc?: string;

  @ManyToMany(() => Role, role => role.apis)
  roles?: Role[];
}
