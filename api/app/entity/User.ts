/*
 * @Author: zhangyang
 * @Date: 2022-06-19 10:01:58
 * @LastEditTime: 2022-06-22 16:32:06
 * @Description:
 */
import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseCreate } from './BaseCreate';
import { Role } from './Role';

@Entity({
  engine: 'InnoDB',
})
export class User extends BaseCreate {
  @Column({ length: '20' })
  username!: string;

  @Column({ length: '11' })
  mobile!: string;

  @Column({ length: '20', default: '' })
  nickname?: string;

  @Column({ default: '' })
  avatar?: string;

  @Column({ length: '50', default: '' })
  introduction?: string;

  @Column({ select: false })
  password!: string;

  @ManyToOne(() => Role, role => role.users, { cascade: true })
  role!: Role;
}
