/*
 * @Author: zhangyang
 * @Date: 2022-06-20 09:02:59
 * @LastEditTime: 2022-06-22 09:34:09
 * @Description:
 */
import { Context } from 'egg';

export default function () {
  return async (ctx: Context, next: any) => {
    await next();
    console.info(`access url: ${ctx.url}`);
  };
}
