/*
 * @Author: zhangyang
 * @Date: 2022-06-21 11:22:40
 * @LastEditTime: 2022-06-28 10:17:31
 * @Description:
 */
import { Role, User, YoungService } from '@app/entity';
import type { CreateUserItem } from 'api/typings/app/init';
import { Paginition } from 'api/typings/app/paginition';

export default class UserService extends YoungService<User, CreateUserItem> {
  protected repo = this.app.dataSource.getRepository(User);

  async create(item: CreateUserItem) {
    const user = new User();
    for (const [key, value] of Object.entries(item)) {
      if (key === 'rid') {
        const role = await this.app.dataSource.getRepository(Role).findOne({
          where: {
            id: value as number,
          },
        });
        if (role) {
          user.role = role;
        } else {
          throw new Error(`roleId: ${value} doesn't exsist`);
        }
      } else {
        // @ts-expect-error
        user[key] = value;
      }
    }
    await this.repo.save(user);
  }

  async findAll(item: CreateUserItem & Paginition) {
    const query = this.repo.createQueryBuilder();
    query.select('*');

    const { noPagination, pageNum, pageSize, username, mobile, status } = item;

    if (!Number.isNaN(status)) {
      query.andWhere(`status = :status`, { status });
    }

    if (username) {
      query.andWhere(`username like '%${username}%'`);
    }

    if (mobile) {
      query.andWhere(`mobile like '%${mobile}%'`);
    }

    const page = Number(pageNum) || 1;
    const size = Number(pageSize) || 10;

    if (!item.noPagination) {
      const skip = (page - 1) * size;

      query.skip(skip);
      query.take(size);
    }

    const total = await query.getCount();
    const list = await query.getRawMany();

    const roleList = await this.app.dataSource.getRepository(Role).find();
    const roleObj: Record<string, string> = {};
    for (const role of roleList) {
      roleObj[role.id] = role.name;
    }
    for (const user of list) {
      delete user.password;
      user.id = Number(user.id);
      user.roleId = Number(user.roleId);
      user.role_name = roleObj[user.roleId];
    }

    return {
      pageNum: page,
      pageSize: size,
      total,
      noPagination,
      list,
    };
  }

  async update(id: number, item: CreateUserItem) {
    const m = await this.getById(id);
    for (const [key, value] of Object.entries(item)) {
      if (value !== undefined && value !== null) {
        if (key === 'rid') {
          const role = await this.app.dataSource.getRepository(Role).findOne({
            where: { id: Number(value) },
          });
          if (role) {
            m.role = role;
          } else {
            throw new Error(`roleId: ${value} 不存在！`);
          }
        } else {
          // @ts-expect-error;
          m[key] = value;
        }
      }
    }
    await this.repo.save(m);
  }

  async getById(id: number) {
    const role = await this.repo.findOne({
      where: { id },
      relations: ['role'],
    });
    if (!role) {
      throw new Error(`userId: ${id} 不存在`);
    }
    return role;
  }
}
