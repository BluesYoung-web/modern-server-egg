/*
 * @Author: zhangyang
 * @Date: 2022-06-20 14:01:35
 * @LastEditTime: 2022-06-29 08:14:26
 * @Description:
 */
import type { Menu, Api, Role, User } from '@app/entity';
import type { useContext } from '@modern-js/runtime/server';

export type CreateMenuItem = Partial<Menu> & { pid?: number };
export type CreateApiItem = Partial<Api> & { rids?: number[] };
export type CreateRoleItem = Partial<Role> & {
  aid?: number[];
  mid?: number[];
};
export type CreateUserItem = Partial<User> & { rid?: number };

export type Ctx = ReturnType<typeof useContext>;

export type Obj = Record<string, any>;
