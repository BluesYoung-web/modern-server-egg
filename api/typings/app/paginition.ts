/*
 * @Author: zhangyang
 * @Date: 2022-06-22 16:27:41
 * @LastEditTime: 2022-06-23 15:11:05
 * @Description:
 */
export type Paginition = {
  pageNum?: number;
  pageSize?: number;
  total?: number;
  noPagination?: boolean;
};
