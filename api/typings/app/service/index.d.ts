// This file is created by egg-ts-helper@1.30.3
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportApi from '../../../app/service/api';
import ExportMenu from '../../../app/service/menu';
import ExportRole from '../../../app/service/role';
import ExportUser from '../../../app/service/user';

type AnyClass = new (...args: any[]) => any;
type AnyFunc<T = any> = (...args: any[]) => T;
type CanExportFunc = AnyFunc<Promise<any>> | AnyFunc<IterableIterator<any>>;
type AutoInstanceType<
  T,
  U = T extends CanExportFunc ? T : T extends AnyFunc ? ReturnType<T> : T,
> = U extends AnyClass ? InstanceType<U> : U;

declare module 'egg' {
  interface IService {
    api: AutoInstanceType<typeof ExportApi>;
    menu: AutoInstanceType<typeof ExportMenu>;
    role: AutoInstanceType<typeof ExportRole>;
    user: AutoInstanceType<typeof ExportUser>;
  }
}
