// This file is created by egg-ts-helper@1.30.3
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportTokenHandler from '../../../app/middleware/tokenHandler';
import ExportTrace from '../../../app/middleware/trace';

declare module 'egg' {
  interface IMiddleware {
    tokenHandler: typeof ExportTokenHandler;
    trace: typeof ExportTrace;
  }
}
