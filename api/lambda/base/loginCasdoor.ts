/*
 * @Author: zhangyang
 * @Date: 2022-06-22 16:54:52
 * @LastEditTime: 2022-06-22 17:14:23
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export const post = async () => {
  const ctx = useContext();

  ctx.logger.info(ctx.request.body);
  // const { code, state } = ctx.request.body;

  return ctx.helper.success({
    msg: '敬请期待！',
    data: ctx.request.body,
  });
};
