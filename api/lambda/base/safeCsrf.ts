/*
 * @Author: zhangyang
 * @Date: 2022-06-22 16:16:45
 * @LastEditTime: 2022-06-22 16:21:31
 * @Description: 获取 csrf 值，所有非 get 请求必须携带 _csrf 字段
 */
import { useContext } from '@modern-js/runtime/server';

export default async () => {
  const ctx = useContext();
  return ctx.helper.success({
    data: {
      csrf: ctx.csrf,
    },
  });
};
