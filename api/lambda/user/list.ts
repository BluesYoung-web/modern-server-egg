/*
 * @Author: zhangyang
 * @Date: 2022-06-28 08:51:06
 * @LastEditTime: 2022-06-28 09:49:43
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export default async () => {
  const ctx = useContext();
  const { pageNum, pageSize, username, mobile, status } = ctx.request.query;

  const data = await ctx.service.user.findAll({
    pageNum: Number(pageNum),
    pageSize: Number(pageSize),
    status: Number(status),
    username,
    mobile,
  });

  return ctx.helper.success({ data });
};
