/*
 * @Author: zhangyang
 * @Date: 2022-06-28 09:56:04
 * @LastEditTime: 2022-06-28 10:00:55
 * @Description:
 */
import md5 from 'md5';
import { useContext } from '@modern-js/runtime/server';

export const post = async () => {
  const ctx = useContext();

  const {
    roleId: rid,
    username = '',
    nickname = '',
    initPassword: pwd = '',
    mobile = '',
  } = ctx.request.body;

  try {
    const { username: u, nickname: n } = ctx.decode;
    await ctx.service.user.create({
      username,
      nickname,
      creator: n || u,
      password: md5(`${ctx.app.config._young.key}${pwd}`),
      mobile,
      rid,
    });
    return ctx.helper.success();
  } catch (error) {
    return ctx.helper.fail({ msg: (error as Error).message });
  }
};
