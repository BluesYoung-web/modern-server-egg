/*
 * @Author: zhangyang
 * @Date: 2022-06-23 17:05:47
 * @LastEditTime: 2022-06-28 10:20:27
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';
import dayjs from 'dayjs';
import md5 from 'md5';

export const patch = async () => {
  const ctx = useContext();
  const id = ctx.params.userId;
  const {
    username,
    nickname,
    mobile,
    roleId: rid,
    status,
    newPassword,
  } = ctx.request.body;

  try {
    await ctx.service.user.update(Number(id), {
      username,
      nickname,
      mobile,
      rid,
      status,
      password: newPassword
        ? md5(`${ctx.app.config._young.key}${newPassword}`)
        : undefined,
      updateAt: dayjs(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
    });
    return ctx.helper.success();
  } catch (error) {
    return ctx.helper.fail({ msg: (error as Error).message });
  }
};
