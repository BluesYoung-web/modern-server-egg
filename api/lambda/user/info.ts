/*
 * @Author: zhangyang
 * @Date: 2022-06-23 09:14:23
 * @LastEditTime: 2022-06-23 09:48:57
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';
import type { CreateUserItem } from 'api/typings/app/init';

export const post = async () => {
  const ctx = useContext();
  const { username } = ctx.decode;
  const user = await ctx.service.user.findOne({ username });
  if (user) {
    const data: CreateUserItem = {
      ...user,
    };
    delete data.password;
    delete data.createdAt;
    delete data.updateAt;
    delete data.creator;
    delete data.status;
    delete data.sort;
    // @ts-expect-error
    delete data.roleId;

    return ctx.helper.success({ data });
  } else {
    return ctx.helper.fail({ msg: '用户不存在！' });
  }
};
