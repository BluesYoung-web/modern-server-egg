/*
 * @Author: zhangyang
 * @Date: 2022-06-23 19:17:37
 * @LastEditTime: 2022-06-28 10:03:06
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export const Delete = async () => {
  const ctx = useContext();
  const { ids } = ctx.request.body;
  await ctx.service.user.deleteBatch(
    ids.split(',').map((i: string) => Number(i)),
  );
  return ctx.helper.success();
};
