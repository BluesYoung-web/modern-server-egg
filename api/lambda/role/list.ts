/*
 * @Author: zhangyang
 * @Date: 2022-06-26 15:15:19
 * @LastEditTime: 2022-06-26 16:34:57
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export default async () => {
  const ctx = useContext();
  const { pageNum, pageSize, status, name, keyword } = ctx.request.query;
  const roleList = await ctx.service.role.findAll({
    pageNum: Number(pageNum),
    pageSize: Number(pageSize),
    status: Number(status),
    name,
    keyword,
  });

  return ctx.helper.success({
    data: roleList,
  });
};
