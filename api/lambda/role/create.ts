/*
 * @Author: zhangyang
 * @Date: 2022-06-26 15:39:00
 * @LastEditTime: 2022-06-26 16:16:00
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';
import type { CreateRoleItem } from 'api/typings/app/init';

export const post = async () => {
  const ctx = useContext();

  const {
    name = '',
    keyword = '',
    desc = '',
    sort = 0,
    status = 1,
  } = ctx.request.body as CreateRoleItem;

  try {
    const { username, nickname } = ctx.decode;
    await ctx.service.role.create({
      name,
      sort,
      creator: nickname || username,
      keyword,
      status,
      desc,
    });
    return ctx.helper.success();
  } catch (error) {
    return ctx.helper.fail({ msg: (error as Error).message });
  }
};
