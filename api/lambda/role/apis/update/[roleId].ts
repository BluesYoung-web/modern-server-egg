/*
 * @Author: zhangyang
 * @Date: 2022-06-27 09:39:37
 * @LastEditTime: 2022-06-27 09:40:43
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export const patch = async () => {
  const ctx = useContext();
  const roleId = Number(ctx.params.roleId);

  if (!roleId) {
    return ctx.helper.fail({ msg: '缺少必要参数！' });
  }

  const { create: add = [], delete: del = [] } = ctx.request.body;
  await ctx.service.role.addApis(roleId, add);
  await ctx.service.role.delApis(roleId, del);
  return ctx.helper.success({
    data: {
      roleId,
      add,
      del,
    },
  });
};
