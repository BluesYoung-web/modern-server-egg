/*
 * @Author: zhangyang
 * @Date: 2022-06-23 17:05:47
 * @LastEditTime: 2022-06-26 16:47:06
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';
import type { CreateRoleItem } from 'api/typings/app/init';
import dayjs from 'dayjs';

export const patch = async () => {
  const ctx = useContext();
  const id = ctx.params.roleId;
  const { name, keyword, desc, sort, status } = ctx.request
    .body as CreateRoleItem;

  try {
    await ctx.service.role.update(Number(id), {
      name,
      keyword,
      desc,
      sort,
      status,
      updateAt: dayjs(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
    });
    return ctx.helper.success();
  } catch (error) {
    return ctx.helper.fail({ msg: (error as Error).message });
  }
};
