/*
 * @Author: zhangyang
 * @Date: 2022-06-23 16:27:14
 * @LastEditTime: 2022-06-23 16:32:57
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export default async () => {
  const ctx = useContext();
  const menus = await ctx.service.menu.getAllTree();
  return ctx.helper.success({ data: menus });
};
