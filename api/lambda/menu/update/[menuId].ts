/*
 * @Author: zhangyang
 * @Date: 2022-06-23 17:05:47
 * @LastEditTime: 2022-06-24 10:26:01
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';
import type { CreateMenuItem } from 'api/typings/app/init';
import dayjs from 'dayjs';

export const patch = async () => {
  const ctx = useContext();
  const id = ctx.params.menuId;
  const {
    parentId: pid,
    name,
    title,
    icon,
    sort,
    status,
    component,
    visible,
  } = ctx.request.body as CreateMenuItem;

  if (component && !component.startsWith('/')) {
    return ctx.helper.fail({ msg: '路径不合法' });
  }

  try {
    await ctx.service.menu.update(Number(id), {
      name,
      title,
      icon,
      sort,
      status,
      component,
      pid,
      visible,
      updateAt: dayjs(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
    });
    return ctx.helper.success();
  } catch (error) {
    return ctx.helper.fail({ msg: (error as Error).message });
  }
};
