/*
 * @Author: zhangyang
 * @Date: 2022-06-27 08:28:54
 * @LastEditTime: 2022-06-27 08:49:41
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export default async () => {
  const ctx = useContext();
  const roleId = Number(ctx.params.roleId);

  if (!roleId) {
    return ctx.helper.fail({ msg: '缺少必要参数！' });
  }

  const accessIds = await ctx.service.role.getAccessMenus(roleId);
  const list = await ctx.service.menu.getAllTree();
  return ctx.helper.success({
    data: {
      accessIds: accessIds.map(i => Number(i.id)),
      list,
    },
  });
};
