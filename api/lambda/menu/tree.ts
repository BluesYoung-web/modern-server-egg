/*
 * @Author: zhangyang
 * @Date: 2022-06-23 09:51:40
 * @LastEditTime: 2022-06-24 10:26:04
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export default async () => {
  const ctx = useContext();
  const { username } = ctx.decode;
  const user = await ctx.service.user.findOne({ username });
  if (user) {
    const id = user.roleId;
    const role = await ctx.service.role.findOne({ id });
    if (role) {
      const menus = await ctx.service.role.getAccessMenusTree(role.id);
      return ctx.helper.success({ data: menus });
    } else {
      return ctx.helper.fail({ msg: '角色不存在！' });
    }
  } else {
    return ctx.helper.fail({ msg: '用户不存在！' });
  }
};
