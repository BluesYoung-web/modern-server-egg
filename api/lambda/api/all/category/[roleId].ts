/*
 * @Author: zhangyang
 * @Date: 2022-06-27 09:19:14
 * @LastEditTime: 2022-06-27 09:35:42
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export default async () => {
  const ctx = useContext();
  const roleId = Number(ctx.params.roleId);

  const apis = await ctx.service.role.getAccessApis(roleId);
  const list = await ctx.service.api.getAllTree();
  return ctx.helper.success({
    data: {
      accessIds: apis.map(i => Number(i.id)),
      list,
    },
  });
};
