/*
 * @Author: zhangyang
 * @Date: 2022-06-29 08:09:19
 * @LastEditTime: 2022-06-29 08:42:39
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export const post = async () => {
  const ctx = useContext();

  const {
    category = '',
    desc = '',
    method = '',
    path = '',
    roleIds: rids,
  } = ctx.request.body;

  if (!path.startsWith('/')) {
    return ctx.helper.fail({ msg: '路径不合法' });
  }

  try {
    const { username: u, nickname: n } = ctx.decode;
    await ctx.service.api.create({
      category,
      desc,
      method,
      path,
      rids,
      creator: n || u,
    });
    return ctx.helper.success();
  } catch (error) {
    return ctx.helper.fail({ msg: (error as Error).message });
  }
};
