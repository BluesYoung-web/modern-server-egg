/*
 * @Author: zhangyang
 * @Date: 2022-06-29 08:03:53
 * @LastEditTime: 2022-06-29 08:03:53
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';

export default async () => {
  const ctx = useContext();
  const { pageNum, pageSize, path } = ctx.request.query;

  const data = await ctx.service.api.findAll({
    pageNum: Number(pageNum),
    pageSize: Number(pageSize),
    path,
  });

  return ctx.helper.success({ data });
};
