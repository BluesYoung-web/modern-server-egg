/*
 * @Author: zhangyang
 * @Date: 2022-06-23 17:05:47
 * @LastEditTime: 2022-06-29 08:36:13
 * @Description:
 */
import { useContext } from '@modern-js/runtime/server';
import dayjs from 'dayjs';

export const patch = async () => {
  const ctx = useContext();
  const id = ctx.params.apiId;
  const { category, desc, method, path, roleIds: rids } = ctx.request.body;

  try {
    await ctx.service.api.update(Number(id), {
      category,
      desc,
      method,
      path,
      rids,
      updateAt: dayjs(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
    });
    return ctx.helper.success();
  } catch (error) {
    return ctx.helper.fail({ msg: (error as Error).message });
  }
};
