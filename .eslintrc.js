/*
 * @Author: zhangyang
 * @Date: 2022-06-20 09:02:59
 * @LastEditTime: 2022-06-21 09:57:07
 * @Description:
 */
module.exports = {
  root: true,
  extends: ['@modern-js'],
  rules: {
    '@typescript-eslint/ban-ts-comment': 'off',
    '@typescript-eslint/type-annotation': 'off',
    '@typescript-eslint/no-empty-interface': 'off',
    '@typescript-eslint/typedef': 'off',
  },
};
